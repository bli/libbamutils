from setuptools import setup, find_packages
#from Cython.Build import cythonize

name = "libbamutils"

# Adapted from Biopython
__version__ = "Undefined"
for line in open("%s/__init__.py" % name):
    if (line.startswith('__version__')):
        exec(line.strip())


setup(
    name=name,
    version=__version__,
    description="Miscellaneous things to inspect and process bam files.",
    author="Blaise Li",
    author_email="blaise.li@normalesup.org",
    license="MIT",
    packages=find_packages(),
    install_requires=[
        "bam25prime @ git+https://gitlab.pasteur.fr/bli/bam25prime.git@ec4aaa5b1add2733c74eaeb9bb881034e702a916",
        "cytoolz",
        # Be careful: libhts depends on libbamutils
        "libhts @ git+https://gitlab.pasteur.fr/bli/libhts.git@36d33ac2c366c0f0702ab50fdcab37261add7b9c",
        "libreads @ git+https://gitlab.pasteur.fr/bli/libreads.git@91db379cd379f8f12fccdd3840d4369b7f09d444"])
    #ext_modules = cythonize("libsmallrna/libsmallrna.pyx"),
    #install_requires=["cytoolz"],
    #zip_safe=False
