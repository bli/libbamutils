# Copyright (C) 2020,2021 Blaise Li
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
from sys import stderr
import warnings
# import numpy
from abc import ABC, abstractmethod
from collections import Counter, deque
from itertools import dropwhile, takewhile
from operator import attrgetter
from cytoolz import merge_with
# To get coordinates of genes
from libhts import BEDTOOLS_VERSION, id_list_gtf2bed
# To collapse aligned reads to their 5' ends
from bam25prime import (
    collapse_and_sort,
    collapse_and_sort_bedtool,
    filter_feature_size)
# bam25prime re-exports those classes from pysam and pybedtools
# to enable simpler dependency management
from bam25prime import AlignmentFile, BedTool
from libreads import bam2fastq


def formatwarning(message, category, filename, lineno, line):  # pylint: disable=W0613
    """Used to format warning messages."""
    return "%s:%s: %s: %s\n" % (filename, lineno, category.__name__, message)


warnings.formatwarning = formatwarning


# To unpack location info from a pybedtools.Interval.
UNPACK = attrgetter("chrom", "start", "end", "strand")
ANTISTRAND = {"+": "-", "-": "+"}


def filter_read_size(alis, min_len=None, max_len=None):
    """
    Skip aligned reads from iterator of :class:`pysam.AlignedSegment` *alis*
    if they are outside the boundaries defined by *min_len* and *max_len*.
    """
    for ali in alis:
        alen = ali.query_length
        if min_len is not None and alen < min_len:
            continue
        if max_len is not None and alen > max_len:
            continue
        yield ali


# To correctly preserve strand information after merging records
# See https://bioinformatics.stackexchange.com/q/15834/292
[major, minor, patch] = map(int, BEDTOOLS_VERSION[1:].split("."))
if (major, minor) >= (2, 27):
    # s=True: Force strandedness.
    # That is, only merge features that are on the same strand
    # Otherwise, strand information is lost
    # With 2.27 and above, it seems this is not enough.
    merge_kwds = {"s": True, "c": [4, 5, 6], "o": "distinct"}
else:
    # TODO: Find a way to have the columns in the correct order
    merge_kwds = {"s": True, "c": [4, 5], "o": "distinct"}


def get_gene_intervals(gene_list_filename, annot_filename, id_kwd=None):
    """
    Obtain the bed coordinates of the genomic regions covered by
    the genes listed in *gene_list_filename*, as annotated in
    *annot_filename*.
    """
    if id_kwd is None:
        id_kwd = "gene_id"
    with open(gene_list_filename, "r") as gene_list_file:
        gene_identifiers = {line.strip() for line in gene_list_file}
    # Deadlock when chaining stream=True?
    # https://daler.github.io/pybedtools/topical-iterators.html
    # intervals = id_list_gtf2bed(
    #     gene_identifiers,
    #     args.annotations, id_kwd=args.id_kwd).sort(
    #         stream=True).merge(stream=True)
    return id_list_gtf2bed(
        gene_identifiers, annot_filename,
        id_kwd=id_kwd).sort().merge(
            **merge_kwds,
            stream=True)


# TODO: allow different upstream and downstream maximum distances
# For CSR1-loaded siRNAs, we expect the cut to happen somewhere
# within the length of the siRNA, so it might be better to look
# from -15 to + 25, for instance
def zip_alis(alis1, alis2, max_dists=None, antisense=True):
    """
    Iterates over two iterators of aligned segments *alis1* and *alis2*
    so that the iteration over *alis2* is driven by that over *alis1*
    in the sense that alignments will be taken in *alis2*
    only in regions surrounding alignments in *alis1*.

    Yields pairs consiting in one alignment from *alis1* and
    the list of those found in its surroundings in *alis2*.

    The input aligned segments should consist in :class:`pybedtools.Interval`s
    of length 1 so that only their five-prime ends are taken into account
    to determine the distance separating them.

    By default, the surroundings of an alignment from *alis1* will
    only contain alignments of opposite strandedness.
    If *antisense* is set to False, then it will only contain
    alignments of the same strandedness.
    """
    if max_dists is None:
        max_dists = {"+": (120, 120), "-": (120, 120)}
    # Will be updated to contain the alignments from alis2
    # that surround the current alignment from alis1,
    # and are on the opposite strand
    antisense_surroundings = {"+": deque([]), "-": deque([])}
    # Note that alis2 needs to be an iterator, otherwise, next will fail.
    # (An iterator can be obtained by calling iter on an iterable.)
    # In case of StopIteration before even iterating alis1,
    # we let it propagate (exit zip_alis)
    ## debug
    # five2s = {"+": defaultdict(list), "-": defaultdict(list)}
    # def record_five2(chrom_info, pos_info, strand_info):
    #     print(pos_info)
    #     five_list = five2s[strand_info]
    #     if five_list[chrom_info]:
    #         assert five_list[chrom_info][-1] <= pos_info
    #     five_list[chrom_info].append(pos_info)
    ##
    try:
        (ref2, five2, _, strand2) = next(alis2)
    except StopIteration:
        return
    # Is the alis2 iterator exhausted?
    exhausted = False
    for (ref1, five1, _, strand1) in alis1:
        # Skip alignments in previous chromosomes
        while ref2 < ref1:
            try:
                (ref2, five2, _, strand2) = next(alis2)
            except StopIteration:
                # We need to catch this to avoid exiting zip_alis too soon.
                exhausted = True
                break
        if ref2 > ref1:
            # There were no alignments in alis2 in the same chromosome
            # yield ((ref1, five1, strand1), [])
            continue
        # Now we have an alignment on the same chromosome
        assert ref2 == ref1
        # TODO: Check coordinates handling
        # Select the appropriate surroundings
        if antisense:
            surroundings = antisense_surroundings[ANTISTRAND[strand1]]
        else:
            surroundings = antisense_surroundings[strand1]
        # Purge surroundings from alignments in the previous chromosomes
        try:
            while surroundings[0][0] < ref1:
                # _ = surroundings.popleft()
                surroundings.popleft()
        except IndexError:
            # surroundings has been emptied
            pass
        (before_dist, after_dist) = max_dists[strand1]
        # Skip upstream alignments
        # Example with before_dist = 4
        #        4321|--->
        # <---|      . skip
        #  <---|     . skip
        #   <---|    . skip
        #    <---|   . OK
        #        432101234
        while ref2 == ref1 and five2 < five1 - before_dist:
            try:
                (ref2, five2, _, strand2) = next(alis2)
                ## debug
                # record_five2(ref2, five2, strand2)
                ##
            except StopIteration:
                # We need to catch this to avoid exiting zip_alis too soon.
                exhausted = True
                break
        # Handle the case where the surroundings is not empty
        # and has only reads from chromosomes after ref1
        if ref2 > ref1:
            # There were no close-enough alignments in alis2
            # in the same chromosome
            # No need to register it:
            # it will be there at the beginning of next iteration
            # antisense_surroundings[strand2].append((ref2, five2, strand2))
            continue
        assert ref2 == ref1
        # Now five2 >= five1 - before_dist
        # Fill the surroundings
        # Example with after_dist = 4
        #        4321|--->
        #    <---|   . append
        #     <---|  . append
        #      <---| . append
        #       <---|. append
        #        <---| append
        #         <---| append
        #          <---| append
        #           <---| append
        #            <---| append
        #            .<---| stop
        #        432101234
        # Problem: We fill for both strands, but using the current value
        # of after_dist. This can over-fill the deque for the other strand.
        while ref2 == ref1 and five2 < five1 + after_dist + 1:
            antisense_surroundings[strand2].append((ref2, five2, strand2))
            try:
                (ref2, five2, _, strand2) = next(alis2)
                ## debug
                # record_five2(ref2, five2, strand2)
                ##
            except StopIteration:
                # We need to catch this to avoid exiting zip_alis too soon.
                exhausted = True
                break
        # Now we are either too far downstream on ref1,
        # or on another chromosome
        # Purge surroundings from upstream alignments, if any
        try:
            while surroundings[0][1] < five1 - before_dist:
                # _ = surroundings.popleft()
                (popped_ref, _, _) = surroundings.popleft()
                assert popped_ref == ref1
                # surroundings.popleft()
        except IndexError:
            # surroundings has been emptied
            pass
        if surroundings:
            # assert sorted(surroundings) == list(surroundings), ", ".join(map(str, surroundings))

            def not_downstream(neighbour):
                """Tells if *neighbour* is not downstream."""
                return neighbour[1] <= five1 + after_dist  # pylint: disable=W0640

            def upstream(neighbour):
                """Tells if *neighbour* is upstream."""
                return neighbour[1] < five1 - before_dist  # pylint: disable=W0640
            real_surroundings = list(takewhile(
                not_downstream,
                dropwhile(upstream, surroundings)))
            # real_surroundings = list(takewhile(
            #     range_tester(five1, before_dist, after_dist),
            #     surroundings))
            # in_range = range_tester(five1, before_dist, after_dist)
            # real_surroundings_2 = [
            #     neighbour for neighbour in surroundings
            #     if in_range(neighbour)]
            # real_surroundings_3 = [
            #     (ref, five, strand)
            #     for (ref, five, strand) in surroundings
            #     if five1 - before_dist <= five <= five1 + after_dist]
            # for (n1, n2, n3) in zip(
            #         real_surroundings,
            #         real_surroundings_2,
            #         real_surroundings_3):
            #     if n1[1] != n2[1]:
            #         print("takewhile-dropwhile not like range_tester: "
            #               f"{n1[1]} != {n2[1]}")
            #         msg = "\n".join([
            #             "surroundings",
            #             ", ".join(map(str, surroundings)),
            #             ", ".join([
            #                 f"five1: {five1}",
            #                 f"before: {before_dist}",
            #                 f"after: {after_dist}"]),
            #             "takewhile-dropwhile:",
            #             ", ".join(map(str, real_surroundings)),
            #             "list-comprehension:",
            #             ", ".join(map(str, real_surroundings_3))])
            #     elif n2[1] != n3[1]:
            #         print("range_tester not like list-comprehension: "
            #               f"{n2[1]} != {n3[1]}")
            #         msg = "\n".join([
            #             "surroundings",
            #             ", ".join(map(str, surroundings)),
            #             ", ".join([
            #                 f"five1: {five1}",
            #                 f"before: {before_dist}",
            #                 f"after: {after_dist}"]),
            #             "range_tester:",
            #             ", ".join(map(str, real_surroundings_2)),
            #             "list-comprehension:",
            #             ", ".join(map(str, real_surroundings_3))])
            #     else:
            #         msg = None
            #     assert n1[1] == n2[1] == n3[1], msg
        else:
            real_surroundings = []
        if real_surroundings:
            for (ref, five, strand) in real_surroundings:
                if ref != ref1:
                    print(f"wrong ref: {ref} != {ref1}", file=stderr)
                if five < five1 - before_dist:
                    print(
                        "too far upstream:"
                        f"{five} ({five-five1} < {-before_dist})",
                        file=stderr)
                if five > five1 + after_dist:
                    print(
                        "too far downstream:",
                        f"{five} ({five-five1} > {after_dist})",
                        file=stderr)
                if antisense:
                    assert ANTISTRAND[strand] == strand1
                else:
                    assert strand == strand1
            yield ((ref1, five1, strand1), real_surroundings)
        if exhausted:
            # raise StopIteration
            return


# TODO: implement better measures (z-scores ?)
def dists_to_ali(ali, neighbours, antisense=True):
    """
    Compute local distance histograms.

    *ali* should be a (chrom, pos, strand) tuple, and the *neighbours* list
    should contain similar tuples with opposite strandedness and should have
    their positions within a certain range of pos (for instance resulting from
    the *max_dists* parameter of :func:`zip_alis`).

    By default, distances will only take into account positions
    on opposite strands.
    If *antisense* is set to False, then distances will only consider
    positions on the same strand.
    """
    (chrom1, pos1, strand1) = ali

    if antisense:
        def compatible(ali2):
            """
            Check that a given neighbouring alignment is a suitable neighbour
            of *ali*.

            No distance checks are made.
            """
            (chrom2, _, strand2) = ali2
            if chrom2 != chrom1 or strand2 == strand1:
                print(chrom2, strand2)
            return chrom2 == chrom1 and strand2 != strand1
    else:
        def compatible(ali2):
            """
            Check that a given neighbouring alignment is a suitable neighbour
            of *ali*.

            No distance checks are made.
            """
            (chrom2, _, strand2) = ali2
            if chrom2 != chrom1 or strand2 != strand1:
                print(chrom2, strand2)
            return chrom2 == chrom1 and strand2 == strand1

    msg = (
        "Some neighbouring reads are not compatible with "
        f"{chrom1}, {pos1}, {strand1}!")
    assert all(compatible(ali2) for ali2 in neighbours), msg
    if strand1 == "+":
        return Counter(pos2 - pos1 for (_, pos2, _) in neighbours)
    return Counter(pos1 - pos2 for (_, pos2, _) in neighbours)


class GenposFile(ABC):
    """
    Base class providing a common interface for *BamFile* and *BedFile*.
    """
    @abstractmethod
    def to_5prime(self, intervals=None, min_len=None, max_len=None, strand=None):
        """
        Iterate over the records and yield only those
        that belong to one of the bed *intervals* and whose size falls
        within the boundaries defined by *min_len* and *max_len*.
        The yielded intervals are represented as
        (chrom, start, stop, strand) tuples.
        They are collapsed at their 5' end.

        The output will be generated in the order obtained when sorting
        *intervals*.

        If *intervals* is None, all genomic regions are considered.
        If *min_len* is None, no minimum read length is imposed.
        If *max_len* is None, no maximum read length is imposed.

        If *strand* is set to None or "any", positions will be considered
        independently of their strandedness relation with the intervals.
        If *strand* is set to "same", only positions on the same strand
        as the intervals will be kept.
        If *strand* is set to "opposite", only positions on the opposite strand
        to the intervals will be kept.
        """
        pass


class BedFile(GenposFile):
    """
    Class representing a Bed formatted file, wrapping a
    :class:`pybedtools.BedTool`.
    """
    def __init__(self, bedpath):
        self.bed = BedTool(bedpath)

    # To be usable with context manager "with" syntax
    # for compatibility with BamFile (not sure this really makes sense)
    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        pass

    def to_5prime(self,
                  intervals=None,
                  min_len=None, max_len=None,
                  strand=None):
        """
        Iterate over the *self.bed* and yield only those features
        that belong to one of the bed *intervals* and whose size falls
        within the boundaries defined by *min_len* and *max_len*.
        The yielded features are represented as
        (chrom, start, stop, strand) tuples.
        They are collapsed at their 5' end.

        The output will be generated in the order obtained when sorting
        *intervals*.

        If *intervals* is None, all genomic regions are considered.
        If *min_len* is None, no minimum read length is imposed.
        If *max_len* is None, no maximum read length is imposed.

        If *strand* is set to None or "any", positions will be considered
        independently of their strandedness relation with the intervals.
        If *strand* is set to "same", only positions on the same strand
        as the intervals will be kept.
        If *strand* is set to "opposite", only positions on the opposite strand
        to the intervals will be kept.
        """
        # :func:`bam25prime.collapse_and_sort_bedtool` preprocesses
        # and re-sorts the records so that we have
        # :class:`pybedtools.Interval`s of
        # length 1, corresponding to their 5' ends.
        # Re-sorting is needed because reversed features will be otherwise
        # placed according to their original end coordinate.
        if min_len is not None or max_len is not None:
            bed_source = collapse_and_sort_bedtool(
                filter_feature_size(self.bed, min_len, max_len))
        else:
            bed_source = collapse_and_sort_bedtool(self.bed)
        if intervals is None:
            if strand is not None:
                warnings.warn(
                    "argument *strand* is not taken into account "
                    "when *intervals* is None.\n")
            for ali in bed_source:
                yield UNPACK(ali)
        else:
            intersect_opts = {"u": True, "f": 1, "sorted": True}
            if strand == "same":
                intersect_opts["s"] = True
            if strand == "opposite":
                intersect_opts["S"] = True
            for ali in bed_source.intersect(
                    intervals.sort(), **intersect_opts):
                yield UNPACK(ali)


class BamFile(GenposFile):
    """
    Class representing a Bam formatted file, wrapping a
    :class:`pysam.AlignmentFile`.
    """
    def __init__(self, bampath):
        self.bam = AlignmentFile(bampath, "rb")

    def __enter__(self):
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.bam.close()

    def chrom_dict(self):
        """
        Return a dictionary associating chromosome names to the corresponding
        lengths.
        """
        return dict(zip(self.bam.references, self.bam.lengths))

    # def mapped_areas(self):
    #     map = {
    #         chrom_name: numpy.zeros(chrom_len, dtype="int")
    #         for (chrom_name, chrom_len) in self.chrom_dict.items()}
    #     for (chrom_name, chrom_map) in map.items():
    #         piles = self.bam.pileup(region=chrom_name)
    #         for pile in piles:
    #             chrom_map[pile.pos] = pile.get_num_aligned()

    # def mapped_areas(self):
    #     map = {
    #         chrom_name: (
    #             sum((1 << pile.pos)
    #                 for pile in self.bam.pileup(region=chrom_name)),
    #             chrom_len)
    #         for (chrom_name, chrom_len) in self.chrom_dict.items()}

    def mapped_length(self):
        """Total genome length covered by at least one read."""
        return sum(1 for _ in self.bam.pileup())

    def bed_to_fastq(self, bed, strand="Any"):
        """
        Iterate over the *self.bam* and yield only those aligned reads
        that belong to the bed-formatted interval *bed*.
        The yielded aligned reads are represented as fastq entries.
        *strand* can be "Any" (default), "Same", or "Opposite".
        """
        [chrom, start, end, _, _, strnd] = bed.strip().split()
        if strand == "Same":
            yield from bam2fastq(
                self.bam, chrom, int(start), int(end), strnd)
        elif strand == "Opposite":
            oppose = {"+": "-", "-": "+"}
            yield from bam2fastq(
                self.bam, chrom, int(start), int(end), oppose[strnd])
        else:
            assert strand == "Any"
            yield from bam2fastq(
                self.bam, chrom, int(start), int(end))

    def bedfile_to_fastq(self, bedfile, strand="Any"):
        """
        Iterate over the *self.bam* and yield only those aligned reads
        that belong to one of the bed intervals present in *bedfile*.
        The yielded aligned reads are represented as fastq entries.
        *strand* can be "Any" (default), "Same", or "Opposite".
        """
        with open(bedfile) as bed_fh:
            if strand == "Same":
                for line in bed_fh:
                    [chrom, start, end, _, _, strnd] = line.strip().split()
                    yield from bam2fastq(
                        self.bam, chrom, int(start), int(end), strnd)

            elif strand == "Opposite":
                oppose = {"+": "-", "-": "+"}
                for line in bed_fh:
                    [chrom, start, end, _, _, strnd] = line.strip().split()
                    yield from bam2fastq(
                        self.bam, chrom, int(start), int(end), oppose[strnd])
            else:
                assert strand == "Any"
                for line in bed_fh:
                    [chrom, start, end, _, _, _] = line.strip().split()
                    yield from bam2fastq(
                        self.bam, chrom, int(start), int(end))

    def to_5prime(self,
                  intervals=None,
                  min_len=None, max_len=None,
                  strand=None):
        """
        Iterate over the *self.bam* and yield only those aligned reads
        that belong to one of the bed *intervals* and whose size falls
        within the boundaries defined by *min_len* and *max_len*.
        The yielded aligned reads are represented as
        (chrom, start, stop, strand) tuples.
        They are collapsed at their 5' end.

        The output will be generated in the order obtained when sorting
        *intervals*.

        If *intervals* is None, all genomic regions are considered.
        If *min_len* is None, no minimum read length is imposed.
        If *max_len* is None, no maximum read length is imposed.

        If *strand* is set to None or "any", positions will be considered
        independently of their strandedness relation with the intervals.
        If *strand* is set to "same", only positions on the same strand
        as the intervals will be kept.
        If *strand* is set to "opposite", only positions on the opposite strand
        to the intervals will be kept.
        """
        # :func:`bam25prime.collapse_and_sort` preprocesses and re-sorts the
        # aligned segments so that we have :class:`pybedtools.Interval`s of
        # length 1, corresponding to their 5' ends.
        # Re-sorting is needed because reverse aligned segments will be
        # otherwise placed according to their original end coordinate.
        if intervals is None:
            if strand is not None:
                warnings.warn(
                    "argument *strand* is not taken into account "
                    "when *intervals* is None.\n")
            if min_len is not None or max_len is not None:
                ali_source = collapse_and_sort(
                    filter_read_size(self.bam.fetch(), min_len, max_len))
            else:
                ali_source = collapse_and_sort(self.bam.fetch())
            for ali in ali_source:
                yield UNPACK(ali)
        else:
            # TODO: Check that sort order of intervals
            # is the same as the aligned segments.
            # Chromosome order should be the same, for instance.
            for interval in intervals.sort():
                # print(interval)
                # I	2880868	2883454
                # [...]
                # 2883513
                # 2883514
                # I	2883495	2893718
                # 2883513
                #
                # I	9488487	9492623
                # [...]
                # 9492587
                # 9492616
                # I	9492629	9494589
                # 9492587
                if min_len is not None or max_len is not None:
                    ali_source = collapse_and_sort(
                        filter_read_size(self.bam.fetch(
                            interval.chrom,
                            interval.start,
                            interval.end), min_len, max_len))
                else:
                    ali_source = collapse_and_sort(self.bam.fetch(
                        interval.chrom,
                        interval.start,
                        interval.end))
                if strand is None or strand == "any":
                    for ali in ali_source:
                        # AlignmentFile.fetch may yield alignments
                        # whose start is upstream interval.start
                        # or
                        # whose end is beyond interval.end
                        # (overlap is sufficient)
                        if ali.start < interval.start:
                            continue
                        if ali.end > interval.end:
                            break
                        yield UNPACK(ali)
                if strand == "same":
                    for ali in ali_source:
                        # AlignmentFile.fetch may yield alignments
                        # whose start is upstream interval.start
                        # or
                        # whose end is beyond interval.end
                        # (overlap is sufficient)
                        if ali.start < interval.start:
                            continue
                        if ali.end > interval.end:
                            break
                        if ali.strand != interval.strand:
                            continue
                        yield UNPACK(ali)
                if strand == "opposite":
                    for ali in ali_source:
                        # AlignmentFile.fetch may yield alignments
                        # whose start is upstream interval.start
                        # or
                        # whose end is beyond interval.end
                        # (overlap is sufficient)
                        if ali.start < interval.start:
                            continue
                        if ali.end > interval.end:
                            break
                        if ali.strand == interval.strand:
                            continue
                        yield UNPACK(ali)


# TODO: allow replacing bam files with bed files,
# using pybedtools.BedTool and collapse_and_sort_bedtool
def read_distances(
        genposf_1, genposf_2,
        intervals=None,
        min_len_1=None, max_len_1=None,
        min_len_2=None, max_len_2=None,
        max_dists=None, antisense=True,
        intvl_strandedness=None):
    """
    Compute the distribution of distances of genomic positions in GenposFile
    object *genposf_2* with respect to those in other GenposFile *genposf_1*.

    If *intervals* is provided, only positions from file *genposf_1*
    belonging to these genomic intervals will be considered.

    If *min_len_1* and / or *max_len_1* are provided, positions from
    *genposf_1* that fall outside those boundaries will not be considered.

    If *min_len_2* and / or *max_len_2* are provided, positions from
    *bampath_2* that fall outside those boundaries will not be considered.

    The distances are those between 5' ends of the positions.

    If *max_dists* is provided, it should be a dictionary
    of pairs (dist_before, dist_after) keyed by strand
    ("+" or "-").
    By default, it is the following:
        max_dists = {"+": (120, 120), "-": (120, 120)}

    This means that for a given position from *genposf_1*,
    positions from *genposf_2* whose 5' end falls outside a [-120, 120] range
    around the 5' end of the position of interest will be ignored.

    By default, distances will only take into account positions
    on opposite strands.
    If *antisense* is set to False, then distances will only consider
    positions on the same strand.

    If *intvl_strandedness* is set to None or "any", positions in *genposf_1*
    will be considered independently of their strandedness relation
    with the intervals in *intervals*.
    If *intvl_strandedness* is set to "same", only positions
    on the same strand as the intervals will be kept.
    If *intvl_strandedness* is set to "opposite", only positions
    on the opposite strand to the intervals will be kept.
    """
    if intvl_strandedness is not None and intervals is None:
        warnings.warn(
            "argument *intvl_strandedness* is not taken into account "
            "when *intervals* is None.\n")
    if max_dists is None:
        max_dists = {"+": (120, 120), "-": (120, 120)}
    # combine the counters
    return merge_with(
        sum,
        # count distances to neighbouring genposf_2 positions
        # for each genposf_1 positions
        (dists_to_ali(ali, neighbours, antisense=antisense) for (ali, neighbours) in zip_alis(
            genposf_2.to_5prime(
                min_len=min_len_2, max_len=max_len_2),
            genposf_1.to_5prime(
                intervals=intervals,
                min_len=min_len_1, max_len=max_len_1,
                strand=intvl_strandedness),
            max_dists=max_dists, antisense=antisense)))


# TODO: make a separate script based on this
def compute_read_distances(
        bampath_1, bampath_2,
        gene_list=None, annotations=None, id_kwd="gene_id",
        antisense=True):
    """
    Compute the distribution of distances of reads in Bam formatted file
    *bampath_2* with respect to reads in Bam formatted file *bampath_1*.

    If provided, *gene_list* should be a file containing gene identifiers.
    It will be used to filter reads from file *bampath_1*, based on the
    coordinates obtained for those genes in the gtf file *annotations*.
    The gene identifiers should correspond to the gtf annotation field
    *id_kwd* (by default, "gene_id")

    By default, distances will only take into account alignments
    on opposite strands.
    If *antisense* is set to False, then distances will only consider
    alignments on the same strand.
    """
    if gene_list:
        if annotations is None:
            warnings.warn(
                "argument *gene_list* requires argument *annotations*.\n")
            return 1
        intervals = get_gene_intervals(
            gene_list, annotations, id_kwd)
    else:
        intervals = None
    with BamFile(bampath_1) as refbam, \
            BamFile(bampath_2) as smallbam:
        return read_distances(
            refbam, smallbam,
            intervals=intervals,
            antisense=antisense)
