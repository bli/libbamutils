__copyright__ = "Copyright (C) 2020-2022 Blaise Li"
__licence__ = "GNU GPLv3"
__version__ = "0.12.1"
from .libbamutils import (
    BamFile, BedFile,
    filter_read_size,
    get_gene_intervals,
    read_distances)
